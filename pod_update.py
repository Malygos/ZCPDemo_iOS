#!/usr/bin/env python
# -*- coding: utf-8 -*-
#0.1.0
import os
import sys
import os.path
import shutil
import xml.dom.minidom
import shutil

# global
SETTING_ZCP_CODE_DIR = ''
DEFAULT_ZCP_CODE_DIR = os.environ['HOME'] + '/ZCPCode/'
ZCP_SOURCE_POD_GIT_ADDRESS = 'https://gitlab.com/Malygos/'
ZCP_SOURCE_POD_SOURCE = ''
ZCP_BINARY_POD_SOURCE = ''


def main():

    # exit(1)

    # 打印当前路径
    # no1. os.getcwd() 方法用于返回当前工作目录
    current_dir = os.getcwd()
    print('======================================================================')
    print('✅ 当前所在路径：' + current_dir)


    # 获取当前仓库服务器地址
    # no1. 1> git remote 不带参数，列出已经存在的远程分支 
    #      2> git remote -v | --verbose 列出详细信息，在每一个名字后面列出其远程url
    #      3> git remote add url   添加一个远程仓库
    current_repo_git_sever = os.popen('git remote -v').readlines()[0].split(' ')[0].split('\t')[1]
    print('✅ 当前仓库关联的git地址：' + current_repo_git_sever)


    # 获取当前分支名
    # no1. python调用Shell脚本，有两种方法：os.system()和os.popen()，前者返回值是脚本的退出状态码，后者的返回值是脚本执行过程中的输出内容
    #      system()用于纯显示的情况，等同于在terminal输入的效果；popen()用于需要使用输出内容的情况
    # no2. git symbolic-ref -q --short 显示当前分支名并忽略错误，返回的是一个数组，如['master\n']
    current_repo_current_branchs = os.popen('git symbolic-ref --short -q HEAD').readlines()
    current_repo_branch = ''
    if len(current_repo_current_branchs) > 0:
        current_repo_branch = current_repo_current_branchs[0].replace('\n', '')
        if len(current_repo_branch) == 0:
            print('❌ 获取当前分支名失败')
            exit(1)
    else:
        print('❌ 获取当前分支名失败')
        exit(1)


    # 当前仓库的当前分支代码拉成最新的
    print('🕘 正在拉取当前分支代码... [' + current_repo_branch + ']')
    code = os.system('git pull origin ' + current_repo_branch)
    if code == 0:
        print('✅ 当前仓库代码已更新到最新')
    else:
        print('❌ 拉取当前仓库代码失败，请检查网络是否通畅...')
        exit(1)

    
    # Code路径，私有库和业务库会被clone到这里
    # no1. os.environ 为环境变量字典，os.environ['HOME'] 值为当前pc用户根目录，也就是'~/'
    zcp_code_dir = func_get_zcp_code_dir()
    if os.path.exists(zcp_code_dir):
        print('✅ Code路径已存在：' + zcp_code_dir)
    else:
        os.mkdir(zcp_code_dir)
        print('✅ Code路径不存在，已创建：' + zcp_code_dir)


    # 检查Manifest仓库中的podfile文件
    manifest_dir = zcp_code_dir + 'Manifest_iOS/'
    manifest_podfile_path = manifest_dir + 'Podfiles/Podfile'
    if not os.path.exists(manifest_dir):
        os.chdir(zcp_code_dir)
        print('✅ Manifest仓库不存在：' + manifest_dir)
        print('🕘 正在克隆Manifest仓库...')
        code = os.system('git clone https://gitlab.com/Malygos/Manifest_iOS.git')
        if code == 0:
            print('✅ Manifest仓库克隆成功')
        else:
            print('❌ Manifest仓库克隆失败')
            exit(1)
        os.chdir(current_dir)
    if os.path.exists(manifest_podfile_path):
        print('✅ Manifest中Podfile文件路径：' + manifest_podfile_path)
    else:
        print('❌ Manifest仓库中无Podfile文件')
        exit(1)


    # 切换Manifest仓库的分支
    os.chdir(manifest_dir)
    print('🕘 正在将Manifest切换到当前项目分支... [' + current_repo_branch + ']')
    manifest_current_branch = os.popen('git symbolic-ref --short -q HEAD').readlines()[0].replace('\n', '')
    if len(current_repo_branch) > 0:
        code = os.system('git checkout ' + current_repo_branch)
        if code == 0:
            manifest_current_branch = current_repo_branch
        else:
            print('❌ 切换Manifest仓库到[' + current_repo_branch + ']分支失败')
            exit(1)


    # 拉取Manifest仓库最新代码
    print('🕘 正在更新Manifest... [' + manifest_current_branch + ']')
    code = os.system('git pull origin ' + manifest_current_branch)
    if code == 0:
        print('✅ Manifest仓库已更新')
    else:
        print('❌ Manifest仓库更新失败')
        exit(1)
    os.chdir(current_dir)


    # 将Manifest仓库中的podfile文件拷贝到当前项目中
    project_podfile_path = current_dir + '/Podfile'
    print('🕘 正在将Podfile文件拷贝至当前项目中...')
    try:
        shutil.copyfile(manifest_podfile_path, current_dir+'/Podfile')
        print ('✅ Podfile文件已成功拷贝至项目中')
    except IOError as e:
        print('❌ 拷贝Podfile文件失败，发生了一个错误：%s' % e)
        exit(1)
    except:
        print('❌ 拷贝Podfile文件失败，发生了一个错误：', sys.exc_info())
        exit(1)


    # 读取podfile内容
    # clone模式：表示需要将私有库源码clone到本地，路径为 func_get_zcp_code_dir 方法的返回值
    # dev模式：表示需要本地开发该库，库将以路径形式接入项目
    business_pods = func_read_podfile_for_business_pods(project_podfile_path)
    private_pods = func_read_podfile_for_private_pods(project_podfile_path)
    debug_private_pods = func_read_podfile_for_debug_private_pods(project_podfile_path)

    # 解析出有 @clone 标记的pods
    need_clone_pods = []
    need_clone_pods.extend(func_find_need_clone_pods(business_pods))
    need_clone_pods.extend(func_find_need_clone_pods(private_pods))
    need_clone_pods.extend(func_find_need_clone_pods(debug_private_pods))
    

    # 拉取需要下载到本地或需要开发的私有库代码
    print ('🕘 正在将依赖的私有库代码拉取到本地')
    # for pod_info in need_clone_pods:
    #     func_git_clone_pod(pod_info['git'], pod_info['branch'])
    print ('✅ 依赖的私有库代码已拉取到本地')


    # 更新私有repos
    private_specs_repo_path = os.environ['HOME'] + '/.cocoapods/repos/zcp-repos'
    if os.path.exists(private_specs_repo_path):
        print('🕘 正在更新私有repos')
        os.chdir(private_specs_repo_path)
        os.system('git pull origin master')
        os.chdir(current_dir)
        print('✅ 私有repos已更新')

    
    # 更新私有二进制repos
    private_specs_repo_binary_path = os.environ['HOME'] + '/.cocoapods/repos/zcp-repos-binary'
    if os.path.exists(private_specs_repo_binary_path):
        print('🕘 正在更新私有二进制repos')
        os.chdir(private_specs_repo_binary_path)
        os.system('git pull origin master')
        os.chdir(current_dir)
        print('✅ 私有二进制repos已更新')
    
    
    # 复写podfile文件
    print('🕘 正在复写Podfile文件')
    func_rewrite_podfile(project_podfile_path)
    print('✅ 已复写Podfile文件')


    # pod update
    print('✅ 开始构建pod项目')
    code = os.system('pod update')
    if code != 0:
        print('❌ 构建pod项目失败')
    else:
        print('✅ 构建pod项目成功')


# ====================================================================================================
# read_podfile
# ====================================================================================================

def func_read_podfile_for_business_pods(podfile_path):
    business_pods_rows = func_read_podfile_designated_content_rows(podfile_path, 'business_pods')
    business_pods = []
    for row in business_pods_rows:
        # TODO: 添加解析代码
        pass
    return business_pods

def func_read_podfile_for_third_party_pods(podfile_path):
    third_party_pods_rows = func_read_podfile_designated_content_rows(podfile_path, 'third_party_pods')
    third_party_pods = []
    for row in third_party_pods_rows:
        if '~>' in row:
            list = row.split(',')
            third_party_pods.append({'name': list[0].split('\'')[1], 'tag': list[1].replace('\'', '').split('~>')[1].strip()})
    return third_party_pods

def func_read_podfile_for_private_pods(podfile_path):
    private_pods_rows = func_read_podfile_designated_content_rows(podfile_path, 'private_pods')
    private_pods = []
    for row in private_pods_rows:
        # 源码私有库
        row = row.strip()
        if row.startswith('source_pod'):
            list = row.split(',')
            a_private_pod_info = {}
            a_private_pod_info['name'] = list[0].split('\'')[1]
            a_private_pod_info['branch'] = list[1].split('\'')[1]
            a_private_pod_info['git'] = ZCP_SOURCE_POD_GIT_ADDRESS + a_private_pod_info['name'] + '.git'
            if len(list) > 2:
                a_private_pod_info['mode'] = list[2].split('\'')[1]
            private_pods.append(a_private_pod_info)
        # 二进制私有库
        if row.startswith('binary_pod'):
            list = row.split(',')
            a_private_pod_info = {}
            a_private_pod_info['name'] = list[0].split('\'')[1]
            a_private_pod_info['version'] = list[1].split('\'')[1]
            a_private_pod_info['branch'] = 'master'
            a_private_pod_info['git'] = ZCP_SOURCE_POD_GIT_ADDRESS + a_private_pod_info['name'] + '.git'
            if len(list) > 2:
                a_private_pod_info['mode'] = list[2].split('\'')[1]
            private_pods.append(a_private_pod_info)
    return private_pods

def func_read_podfile_for_debug_third_party_pods(podfile_path):
    debug_third_party_pods_rows = func_read_podfile_designated_content_rows(podfile_path, 'debug_third_party_pods')
    debug_third_party_pods = []
    for row in debug_third_party_pods_rows:
        if '~>' in row:
            list = row.split(',')
            debug_third_party_pod_dict = {}
            debug_third_party_pod_dict['name'] = list[0].split('\'')[1]
            debug_third_party_pod_dict['tag'] = list[1].replace('\'', '').split('~>')[1].strip()
            debug_third_party_pods.append(debug_third_party_pod_dict)

    return debug_third_party_pods

def func_read_podfile_for_debug_private_pods(podfile_path):
    debug_private_pods_rows = func_read_podfile_designated_content_rows(podfile_path, 'debug_private_pods')
    debug_private_pods = []
    for row in debug_private_pods_rows:
        # TODO: 添加解析代码
        print(row)
        pass
    return debug_private_pods

# 返回podfile文件中指定pods类型的行
def func_read_podfile_designated_content_rows(podfile_path, designated_pods_name):
    read_file_Handle = open(podfile_path)
    podfile_contents = read_file_Handle.read()
    read_file_Handle.close()

    podfile_contents_list = podfile_contents.split('\n')
    designated_pods_rows = []
    find_designated_pods_flag = 0
    designated_pods_name = 'def ' + designated_pods_name

    for podfile_row in podfile_contents_list:
        if designated_pods_name in podfile_row:
            find_designated_pods_flag = 1
            continue
        if 'end' == podfile_row and find_designated_pods_flag:
            break
        if find_designated_pods_flag:
            designated_pods_rows.append(podfile_row)
    return designated_pods_rows

def func_find_need_clone_pods(pods):
    need_clone_pods = []
    for pod_info in pods:
        if ('mode' in pod_info.keys()) and (pod_info['mode'] == 'clone' or pod_info['mode'] == 'dev'):
            need_clone_pods.append(pod_info)
    return need_clone_pods

# ====================================================================================================
# other
# ====================================================================================================

def func_get_zcp_code_dir():
    if len(SETTING_ZCP_CODE_DIR) > 0:
        return SETTING_ZCP_CODE_DIR
    else:
        return DEFAULT_ZCP_CODE_DIR

def func_log_list(list):
    for row in list:
        print(row)

def func_git_clone_pod(git_repo_sever, project_branch):
    
    old_path = os.getcwd()
    zcp_code_dir = func_get_zcp_code_dir()

    if not os.path.exists(zcp_code_dir):
        os.mkdir(zcp_code_dir)

    project_branch_split = git_repo_sever.split('/')
    project_name = project_branch_split[len(project_branch_split) - 1].replace('.git', '')
    project_path = zcp_code_dir + project_name

    print('======================================================================')
    print('                         🦊 ' + project_name)
    print('======================================================================')

    if not os.path.exists(project_path):
        os.chdir(zcp_code_dir)
        print('> 🦊 git clone')
        os.system('git clone ' + git_repo_sever)
        print('')
    os.chdir(project_path)

    print('> 🦊 git stash')
    os.system('git status')
    os.system('git stash')
    os.system('git reset --hard')
    print('')

    print('> 🦊 git checkout ' + project_branch)
    os.system('git tag -l | xargs git tag -d')
    os.system('git fetch')
    os.system('git checkout master')
    os.system('git pull origin master')
    os.system('git checkout ' + project_branch)
    print('')
    
    current_repo_current_branchs = os.popen('git symbolic-ref --short -q HEAD').readlines()
    if len(current_repo_current_branchs) > 0:
        project_branch = current_repo_current_branchs[0]
        
        print('> 🦊 git pull origin/' + project_branch)
        os.system('git fetch --all')
        os.system('git reset --hard origin/'+project_branch)
        os.system('git pull origin ' + project_branch)
        print('')
    
    print('> 🦊 git log --stat -2 | head -30')
    os.system('git log --stat -2 | head -30')
    print('')

    project_branch = os.popen('git symbolic-ref --short -q HEAD').readlines()[0]
    print('> 🦊 当前的仓库是：' + git_repo_sever + ' 分支是：' + project_branch)
    print('> ✅ ' + project_name + '拉取并更新成功')
    print('')

    # 恢复path
    os.chdir(old_path)

def func_rewrite_podfile(podfile_path):
    old_path = os.getcwd()

    read_file_Handle = open(podfile_path)
    podfile_contents = read_file_Handle.read()
    read_file_Handle.close()

    podfile_contents_rows = podfile_contents.split('\n')
    new_podfile_contents = ''
    
    for podfile_row in podfile_contents_rows:
        if podfile_row.strip().startswith('source_pod'):
            isDevMode = (':mode => \'dev\'' in podfile_row)
            if isDevMode:
                list = podfile_row.split(':branch')
                spec_name = list[0].split('\'')[1]
                podfile_row = list[0] + ':path => \'~/ZCPCode/' + spec_name + '/\''
            else:
                podfile_row = '    ' + podfile_row.split(', :mode =>')[0].strip()
        if podfile_row.strip().startswith('binary_pod'):
                podfile_row = '    ' + podfile_row.split(', :mode =>')[0].strip()
        new_podfile_contents = new_podfile_contents + podfile_row + '\n'

    write_file_Handle = open(podfile_path, 'w')
    write_file_Handle.write(new_podfile_contents)
    write_file_Handle.close()


if __name__ == '__main__':
    main()