//
//  SMLagMonitor.h
//
//  Created by DaiMing on 16/3/28.
//

#import <Foundation/Foundation.h>

@interface SMLagMonitor : NSObject

+ (instancetype)shareInstance;

- (void)beginMonitor; //开始监视卡顿
- (void)endMonitor;   //停止监视卡顿

// BeforeSources 后和 AfterWaiting 后这两个状态区间时间能够检测到大部分场景的UI卡顿
// 但某些场景是在 beforeWaiting 后执行的，当发生卡顿时会监测不到
// 那么为什么不用新号量监控 BeforeWaiting 呢？
// 因为大部分正常情况下比如不操作界面时runloop会进入休眠，此时监测到runloop最后的状态就是 BeforeWaiting。AfterWaiting会在runloop唤醒时才会发出。所以监测BeforeWaiting没意义。
// 使用下面的方法，将处于beforeWaiting状态下执行的代码包裹起来，即可判断beforeWaiting局部状态下的耗时代码。
@property (nonatomic, assign) BOOL beforeWaitingMonitor;
- (void)monitorBeforeWaitingBlock:(dispatch_block_t)block;

@end
