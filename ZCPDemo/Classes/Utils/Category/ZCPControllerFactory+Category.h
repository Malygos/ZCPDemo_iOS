//
//  ZCPControllerFactory+Category.h
//  Demo
//
//  Created by 朱超鹏 on 2017/10/10.
//  Copyright © 2017年 zcp. All rights reserved.
//

#import <ZCPRouter/ZCPRouter.h>

@interface ZCPControllerFactory (Category)

- (UINavigationController *)generateCustomStack;

@end
