//
//  ZCPDemoBaseTableViewController.m
//  Demo
//
//  Created by 朱超鹏 on 2017/10/10.
//  Copyright © 2017年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCPDemoBaseTableViewController.h"

@interface ZCPDemoBaseTableViewController ()

@end

@implementation ZCPDemoBaseTableViewController

@synthesize formerViewController = _formerViewController;
@synthesize latterViewController = _latterViewController;

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.frame = self.view.bounds;
}

#pragma mark - tableview

- (void)constructData {
    if (!self.isSingleSectionTableView) {
        return;
    }
    ZCPTableViewSingleSectionDataSource *dataSource = (ZCPTableViewSingleSectionDataSource *)self.tableViewDataSource;
    [dataSource.cellViewModelArray removeAllObjects];
    
    for (NSDictionary *infoDict in self.infoArr) {
        ZCPTableViewSingleTitleCellViewModel *viewModel = [[ZCPTableViewSingleTitleCellViewModel alloc] init];
        viewModel.titleString = infoDict[@"title"];
        viewModel.titleFont = [UIFont boldSystemFontOfSize:18];
        viewModel.cellHeight = @(50);
        viewModel.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
        viewModel.useChangeObserver = YES;
        [dataSource.cellViewModelArray addObject:viewModel];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.infoArr[indexPath.row][@"identifier"];
    if (identifier.length > 0) {
        [[ZCPNavigator sharedInstance] gotoViewWithIdentifier:identifier queryForInit:nil  propertyDictionary:nil];
        return;
    }
    
    NSString *class = self.infoArr[indexPath.row][@"class"];
    if (class.length > 0) {
        UIViewController *vc = [[NSClassFromString(class) alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
}

#pragma mark - getters and setters

- (NSMutableArray *)infoArr {
    if (_infoArr == nil) {
        _infoArr = [NSMutableArray array];
    }
    return _infoArr;
}

@end
