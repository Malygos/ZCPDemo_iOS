//
//  ZCPDemoBaseTableViewController.h
//  Demo
//
//  Created by 朱超鹏 on 2017/10/10.
//  Copyright © 2017年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZCPListView/ZCPListView.h>
#import <ZCPRouter/ZCPRouter.h>

@interface ZCPDemoBaseTableViewController : ZCPTableViewController <ZCPNavigatorProtocol>

@property (nonatomic, strong) NSMutableArray *infoArr;

@end
