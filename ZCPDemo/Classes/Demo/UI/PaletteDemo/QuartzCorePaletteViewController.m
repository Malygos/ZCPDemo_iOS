//
//  QuartzCorePaletteViewController.m
//  Demo
//
//  Created by zhuchaopeng on 16/10/10.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "QuartzCorePaletteViewController.h"
#import "TouchDrawView.h"

@interface QuartzCorePaletteViewController ()

@property (weak, nonatomic) IBOutlet TouchDrawView *paletteView;

@end

@implementation QuartzCorePaletteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}


@end
