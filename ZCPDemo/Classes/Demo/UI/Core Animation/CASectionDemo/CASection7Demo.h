//
//  CASection7Demo.h
//  Demo
//
//  Created by 朱超鹏 on 2018/8/20.
//  Copyright © 2018年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

// ----------------------------------------------------------------------
#pragma mark - section7 隐式动画
// ----------------------------------------------------------------------
@interface CASection7Demo : UIView

// 隐式动画
// 事务
// 完成块
// 图层行为
// 呈现与模型

@end
