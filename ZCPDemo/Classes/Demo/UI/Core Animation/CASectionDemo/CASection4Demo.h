//
//  CASection4Demo.h
//  Demo
//
//  Created by 朱超鹏 on 2018/8/9.
//  Copyright © 2018年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

// ----------------------------------------------------------------------
#pragma mark - section4 视觉效果
// ----------------------------------------------------------------------
@interface CASection4Demo : UIView

// 圆角
// 图层边框
// 阴影
// 图层蒙版
// 拉伸过滤

@end
