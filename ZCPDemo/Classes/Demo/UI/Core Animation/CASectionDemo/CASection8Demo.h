//
//  CASection8Demo.h
//  Demo
//
//  Created by 朱超鹏 on 2018/8/20.
//  Copyright © 2018年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

// ----------------------------------------------------------------------
#pragma mark - section8 显式动画
// ----------------------------------------------------------------------
@interface CASection8Demo : UIView

// 属性动画
// 动画组
// 过渡
// 对图层树的动画
// 自定义过渡动画
// 在动画过程中取消动画

@end
