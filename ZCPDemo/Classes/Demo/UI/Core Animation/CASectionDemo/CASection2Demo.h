//
//  CASection2Demo.h
//  Demo
//
//  Created by 朱超鹏 on 2018/8/9.
//  Copyright © 2018年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

// ----------------------------------------------------------------------
#pragma mark - section2 寄宿图
// ----------------------------------------------------------------------

@interface CASection2Demo : UIView

// 通过设置寄宿图来显示一张图片
// 使用contentRect属性实现图片拼合（image sprites）
// 使用contentsCenter属性拉伸图片
// CALayerDelegate

@end
