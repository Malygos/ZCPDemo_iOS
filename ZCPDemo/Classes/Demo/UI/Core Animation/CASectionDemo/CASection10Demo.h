//
//  CASection10Demo.h
//  Demo
//
//  Created by 朱超鹏 on 2018/8/27.
//  Copyright © 2018年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

// ----------------------------------------------------------------------
#pragma mark - section10 缓冲
// ----------------------------------------------------------------------
@interface CASection10Demo : UIView

// 动画速度
// 关键帧动画的动画速度
// 绘制缓冲曲线
// 根据缓冲曲线绘制关键帧动画的所有帧

@end
