//
//  UIView+Elastic.h
//  Demo
//
//  Created by 朱超鹏 on 2018/6/27.
//  Copyright © 2018年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewElasticHelper.h"

/// 橡皮筋效果分类
@interface UIView (Elastic)

@property (nonatomic, strong) UIViewElasticHelper *elasticHelper;

@end
