//
//  IOS12ICONAnimationButton1.h
//  Demo
//
//  Created by 朱超鹏 on 2018/7/19.
//  Copyright © 2018年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IOS12ICONAnimationButton1 : UIControl

/// 开始动画
- (void)startAnimation;

/// 停止动画
- (void)stopAnimation;

@end
