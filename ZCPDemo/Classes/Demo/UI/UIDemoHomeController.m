//
//  UIDemoHomeController.m
//  Demo
//
//  Created by apple on 16/6/14.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "UIDemoHomeController.h"

@implementation UIDemoHomeController

@synthesize infoArr = _infoArr;

- (NSMutableArray *)infoArr {
    if (_infoArr == nil) {
        _infoArr = [NSMutableArray arrayWithObjects:
                    @{@"title": @"Core Animation"       , @"identifier": APPURL_VIEW_IDENTIFIER_COREANIMATION},
                    @{@"title": @"动画"                  , @"identifier": APPURL_VIEW_IDENTIFIER_ANIMATION},
                    @{@"title": @"SizeToFitDemo"        , @"identifier": APPURL_VIEW_IDENTIFIER_SIZETOFIT},
                    @{@"title": @"FontDemo"             , @"identifier": APPURL_VIEW_IDENTIFIER_FONT},
                    @{@"title": @"Palette"              , @"identifier": APPURL_VIEW_IDENTIFIER_PALETTE},
                    @{@"title": @"QiPaoDemo"            , @"identifier": APPURL_VIEW_IDENTIFIER_QIPAO},
                    @{@"title": @"虚线"                  , @"identifier": APPURL_VIEW_IDENTIFIER_DASHED},
                    @{@"title": @"CollectionViewDemo"   , @"identifier": APPURL_VIEW_IDENTIFIER_COLLECTION},
                    @{@"title": @"Alert"                , @"identifier": APPURL_VIEW_IDENTIFIER_ALERT},
                    @{@"title": @"UIButton"             , @"class": @"ButtonDemoHomeVC"},
                    nil];
    }
    return _infoArr;
}

@end
