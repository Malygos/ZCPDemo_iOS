//
//  ButtonDemoHomeVC.m
//  ZCPDemo
//
//  Created by 朱超鹏(平安健康互联网健康研究院) on 2020/7/7.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "ButtonDemoHomeVC.h"
#import <ZCPCategory/ZCPCategory+UIKit.h>
#import <UIButton+WebCache.h>

@interface ButtonDemoHomeVC ()

@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic, strong) UISlider *widthSlider;
@property (nonatomic, strong) UISlider *heightSlider;
@property (nonatomic, strong) UITextField *widthTF;
@property (nonatomic, strong) UITextField *heightTF;
@property (nonatomic, strong) UILabel *subImageFrameLabel;
@property (nonatomic, strong) UILabel *subTitleFrameLabel;
@property (nonatomic, strong) UIButton *button;

@end

@implementation ButtonDemoHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.needsTapToDismissKeyboard = @YES;
    
    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.widthSlider];
    [self.view addSubview:self.heightSlider];
    [self.view addSubview:self.widthTF];
    [self.view addSubview:self.heightTF];
    [self.view addSubview:self.subImageFrameLabel];
    [self.view addSubview:self.subTitleFrameLabel];
    [self.view addSubview:self.button];
    
    self.widthSlider.value = 100;
    self.heightSlider.value = 100;
    self.widthTF.text = @"100";
    self.heightTF.text = @"100";
    
    self.button.frame = CGRectMake(0, 0, 100, 100);
    self.button.center = self.view.center;
    
    [self.button.imageView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    [self.button.titleLabel addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.widthSlider.frame = CGRectMake(0, 0, SCREENWIDTH, 30);
    self.heightSlider.frame = CGRectMake(0, 35, SCREENWIDTH, 30);
    self.widthTF.frame = CGRectMake(0, 70, SCREENWIDTH/3, 30);
    self.heightTF.frame = CGRectMake(0, 105, SCREENWIDTH/3, 30);
    self.subImageFrameLabel.frame = CGRectMake(SCREENWIDTH/3, 70, SCREENWIDTH*2/3, 30);
    self.subTitleFrameLabel.frame = CGRectMake(SCREENWIDTH/3, 105, SCREENWIDTH*2/3, 30);
    self.button.frame = CGRectMake(0, 0, self.widthTF.text.floatValue, self.heightTF.text.floatValue);
    self.button.center = self.view.center;
    self.segmentControl.frame = CGRectMake(0, 140, SCREENWIDTH, 30);
}

#pragma mark - observer

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    CGRect newValue = [change[NSKeyValueChangeOldKey] CGRectValue];
    
    if (object == self.button.imageView) {
        self.subImageFrameLabel.text = [NSString stringWithFormat:@"%.2lf %.2lf %.2lf %.2lf", newValue.origin.x, newValue.origin.y, newValue.size.width, newValue.size.height];
    } else if (object == self.button.titleLabel) {
        self.subTitleFrameLabel.text = [NSString stringWithFormat:@"%.2lf %.2lf %.2lf %.2lf", newValue.origin.x, newValue.origin.y, newValue.size.width, newValue.size.height];
    }
    
    CGSize imageSize = self.button.imageView.size;
    CGSize titleSize = self.button.titleLabel.size;
    CGFloat gap = 10;
    
    [self.button updateSubviewFrameWithTitleSize:titleSize imageSize:imageSize position:self.segmentControl.selectedSegmentIndex gap:gap];
    
    [self.view setNeedsLayout];
    
    if (self.segmentControl.selectedSegmentIndex == 0) {
        NSLog(@"leftGap:%.2lf gap:%.2lf rightGap:%.2lf", self.button.imageView.left, (self.button.titleLabel.left - self.button.imageView.right), (self.button.width - self.button.titleLabel.right));
    } else if (self.segmentControl.selectedSegmentIndex == 1) {
        NSLog(@"leftGap:%.2lf gap:%.2lf rightGap:%.2lf", self.button.titleLabel.left, (self.button.imageView.left - self.button.titleLabel.right), (self.button.width - self.button.imageView.right));
    } else if (self.segmentControl.selectedSegmentIndex == 2) {
        NSLog(@"imageHGap(%.2lf, %.2lf) titleHGap(%.2lf, %.2lf)", self.button.imageView.left, (self.button.width - self.button.imageView.right), self.button.titleLabel.left, (self.button.width - self.button.titleLabel.right));
        NSLog(@"topGap:%.2lf gap:%.2lf bottomGap:%.2lf", self.button.imageView.top, (self.button.titleLabel.top - self.button.imageView.bottom), (self.button.height - self.button.titleLabel.bottom));
    } else if (self.segmentControl.selectedSegmentIndex == 3) {
        NSLog(@"imageHGap(%.2lf, %.2lf) titleHGap(%.2lf, %.2lf)", self.button.imageView.left, (self.button.width - self.button.imageView.right), self.button.titleLabel.left, (self.button.height - self.button.titleLabel.right));
        NSLog(@"topGap:%.2lf gap:%.2lf bottomGap:%.2lf", self.button.titleLabel.top, (self.button.imageView.top - self.button.titleLabel.bottom), (self.button.height - self.button.imageView.bottom));
    }
}

#pragma mark - event response

- (void)segmentControlValueChanged:(UISegmentedControl *)segmentControl {
    self.button.imageView.frame = self.button.imageView.frame;
    [self.view setNeedsLayout];
}

- (void)slider:(UISlider *)slider {
    if (slider == self.widthSlider) {
        self.button.width = slider.value;
        self.button.center = self.view.center;
        self.widthTF.text = [@(slider.value) stringValue];
    } else {
        self.button.height = slider.value;
        self.button.center = self.view.center;
        self.heightTF.text = [@(slider.value) stringValue];
    }
}

- (void)click {
    self.widthSlider.value = self.widthTF.text.floatValue;
    self.heightSlider.value = self.heightTF.text.floatValue;
    self.button.width = self.widthTF.text.floatValue;
    self.button.height = self.heightTF.text.floatValue;
    self.button.center = self.view.center;
}

#pragma mark - getters

- (UISegmentedControl *)segmentControl {
    if (!_segmentControl) {
        _segmentControl = [[UISegmentedControl alloc] initWithItems:@[@"左图右字", @"右图左字", @"上图下字", @"下图上字"]];
        [_segmentControl addTarget:self action:@selector(segmentControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}

- (UISlider *)widthSlider {
    if (!_widthSlider) {
        _widthSlider = [[UISlider alloc] init];
        _widthSlider.minimumValue = 0;
        _widthSlider.maximumValue = SCREENWIDTH;
        [_widthSlider addTarget:self action:@selector(slider:) forControlEvents:UIControlEventValueChanged];
    }
    return _widthSlider;
}

- (UISlider *)heightSlider {
    if (!_heightSlider) {
        _heightSlider = [[UISlider alloc] init];
        _heightSlider.minimumValue = 0;
        _heightSlider.maximumValue = SCREENWIDTH;
        [_heightSlider addTarget:self action:@selector(slider:) forControlEvents:UIControlEventValueChanged];
    }
    return _heightSlider;
}

- (UITextField *)widthTF {
    if (!_widthTF) {
        _widthTF = [[UITextField alloc] init];
        _widthTF.layer.borderColor = [UIColor redColor].CGColor;
        _widthTF.layer.borderWidth = 1;
    }
    return _widthTF;
}

- (UITextField *)heightTF {
    if (!_heightTF) {
        _heightTF = [[UITextField alloc] init];
        _heightTF.layer.borderColor = [UIColor redColor].CGColor;
        _heightTF.layer.borderWidth = 1;
    }
    return _heightTF;
}

- (UILabel *)subImageFrameLabel {
    if (!_subImageFrameLabel) {
        _subImageFrameLabel = [[UILabel alloc] init];
        _subImageFrameLabel.layer.borderColor = [UIColor greenColor].CGColor;
        _subImageFrameLabel.layer.borderWidth = 1;
    }
    return _subImageFrameLabel;
}

- (UILabel *)subTitleFrameLabel {
    if (!_subTitleFrameLabel) {
        _subTitleFrameLabel = [[UILabel alloc] init];
        _subTitleFrameLabel.layer.borderColor = [UIColor greenColor].CGColor;
        _subTitleFrameLabel.layer.borderWidth = 1;
    }
    return _subTitleFrameLabel;
}
- (UIButton *)button {
    if (!_button) {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.layer.borderColor = [UIColor redColor].CGColor;
        _button.layer.borderWidth = 1;
        _button.titleLabel.layer.borderWidth = 1;
        _button.titleLabel.layer.borderColor = [UIColor redColor].CGColor;
        _button.imageView.layer.borderWidth = 1;
        _button.imageView.layer.borderColor = [UIColor redColor].CGColor;
        [_button setTitle:@"哈哈哈" forState:UIControlStateNormal];
        [_button setImage:[UIImage imageNamed:@"tab_home_normal"] forState:UIControlStateNormal];
        [_button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }
    return _button;
}

@end
