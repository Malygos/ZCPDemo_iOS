//
//  TBDemoVC2.m
//  Demo
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "TBDemoVC2.h"

@interface TBDemoVC2 ()

@end

@implementation TBDemoVC2

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

@end
