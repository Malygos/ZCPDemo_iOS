//
//  TBDemoVC1.m
//  Demo
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "TBDemoVC1.h"

@interface TBDemoVC1 ()

@end

@implementation TBDemoVC1

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

@end
