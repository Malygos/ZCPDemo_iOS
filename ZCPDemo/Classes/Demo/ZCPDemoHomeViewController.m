//
//  ZCPDemoHomeViewController.m
//  Demo
//
//  Created by 朱超鹏 on 2017/10/10.
//  Copyright © 2017年 zcp. All rights reserved.
//

#import "ZCPDemoHomeViewController.h"

@implementation ZCPDemoHomeViewController

@synthesize infoArr = _infoArr;

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = @"Demo";
}

- (NSMutableArray *)infoArr {
    if (_infoArr == nil) {
        _infoArr = [NSMutableArray arrayWithObjects:
                    @{@"title": @"临时测试",
                      @"identifier": APPURL_VIEW_IDENTIFIER_TEMPTESTHOME},
                    @{@"title": @"LayoutDemo",
                      @"identifier": APPURL_VIEW_IDENTIFIER_LAYOUTDEMO},
                    @{@"title": @"UIDemo",
                      @"identifier": APPURL_VIEW_IDENTIFIER_UIHOME},
                    @{@"title": @"CameraAndAlbumDemo",
                      @"identifier": APPURL_VIEW_IDENTIFIER_ALBUMHOME},
                    @{@"title": @"OtherDemo",
                      @"identifier": APPURL_VIEW_IDENTIFIER_OTHERHOME},
                    @{@"title": @"二维码扫描"             ,
                      @"identifier": APPURL_VIEW_IDENTIFIER_QRCODEHOME},
                    @{@"title": @"扫雷"                  ,
                      @"identifier": APPURL_VIEW_IDENTIFIER_MINESWEEPER},
                    
                    @{@"title": @"锁",
                      @"identifier": APPURL_VIEW_IDENTIFIER_LOCK},
        
                    @{@"title": @"NetWorkDemo",
                      @"identifier": APPURL_VIEW_IDENTIFIER_NETWORKHOME},
                    @{@"title": @"WebViewDemo",
                      @"identifier": APPURL_VIEW_IDENTIFIER_WEBHOME},
                    @{@"title": @"PageVCDemo",
                      @"identifier": APPURL_VIEW_IDENTIFIER_PAGEVCHOME},
                    
                    @{@"title": @"PhotoCarouselDemo",
                      @"identifier": APPURL_VIEW_IDENTIFIER_PHOTOCAROUSELHOME},
                    @{@"title": @"TabBarDemo",
                      @"identifier": APPURL_VIEW_IDENTIFIER_TABBARHOME},
                    
                    @{@"title": @"国际化",
                      @"identifier": APPURL_VIEW_IDENTIFIER_INTERNATIONALHOME},
                    @{@"title": @"地图&定位",
                      @"identifier": APPURL_VIEW_IDENTIFIER_MAPHOME},
                    @{@"title": @"日志",
                      @"identifier": APPURL_VIEW_IDENTIFIER_LOG},
                    @{@"title": @"UI卡顿监控",
                      @"identifier": APPURL_VIEW_IDENTIFIER_UISTUCKMONITOR}, nil];
    }
    return _infoArr;
}

@end
