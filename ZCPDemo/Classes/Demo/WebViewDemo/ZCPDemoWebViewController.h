//
//  ZCPDemoWebViewController.h
//  Demo
//
//  Created by zcp on 2019/6/4.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZCPWebView/ZCPWebView.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCPDemoWebViewController : ZCPBaseWebViewController

@end

NS_ASSUME_NONNULL_END
