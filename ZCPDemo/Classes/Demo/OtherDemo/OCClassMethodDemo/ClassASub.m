//
//  ClassASub.m
//  AccessOCPrivateMethod
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "ClassASub.h"

@implementation ClassASub

- (void)privateMethodA {
    NSLog(@"privateMethodSubA!");
}

@end
