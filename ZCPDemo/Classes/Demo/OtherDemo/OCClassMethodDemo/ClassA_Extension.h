//
//  ClassA_Extension.h
//  AccessOCPrivateMethod
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "ClassA.h"

@interface ClassA ()

- (void)extensionMethod;

@end
