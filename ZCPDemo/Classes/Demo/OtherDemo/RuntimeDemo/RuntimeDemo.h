//
//  RuntimeDemo.h
//  Demo
//
//  Created by apple on 16/3/10.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RuntimeDemo : NSObject

- (void)run;

@end
