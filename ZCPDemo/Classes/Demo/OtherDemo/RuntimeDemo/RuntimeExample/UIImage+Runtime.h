//
//  UIImage+Runtime.h
//  Demo
//
//  Created by 朱超鹏(外包) on 17/2/13.
//  Copyright © 2017年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Runtime)

+ (void)openUIImageRuntimeTest;
+ (UIImage *)custom_imageNamed:(NSString *)name;

@end
