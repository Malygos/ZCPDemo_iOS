//
//  ObjcMessageDemo.h
//  Demo
//
//  Created by 朱超鹏 on 2018/10/9.
//  Copyright © 2018年 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjcMessageDemo : NSObject

@end


#// ----------------------------------------------------------------------
#pragma mark - test class
// ----------------------------------------------------------------------

@interface OMObject : NSObject

@end

@interface OMAnimal : NSObject

@end

@interface OMPerson : NSObject

@end


