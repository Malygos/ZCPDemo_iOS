//
//  NSString+Demo.h
//  emoji
//
//  Created by apple on 16/6/8.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Demo)

+ (instancetype)stringToUTF16:(NSString *)string;
+ (instancetype)stringToUTF8:(NSString *)string;
+ (instancetype)stringToUnicode:(NSString *)string;

@end
