//
//  BookPageViewController.h
//  Demo
//
//  Created by 朱超鹏 on 2017/6/12.
//  Copyright © 2017年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookPageViewController : UIViewController

- (void)setText:(NSString *)text;

@end
