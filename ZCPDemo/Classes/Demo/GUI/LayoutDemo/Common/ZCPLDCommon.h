//
//  ZCPLDCommon.h
//  Demo
//
//  Created by zcp on 2019/5/14.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZCPLDTagView.h"
#import "ZCPLDEntranceView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZCPLDCommon : NSObject

+ (NSArray *)textTemplates;

@end

NS_ASSUME_NONNULL_END
