//
//  CameraAndAlbumDemoHomeController.m
//  CameraAndAlbumDemo
//
//  Created by apple on 16/3/1.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "CameraAndAlbumDemoHomeController.h"
#import "PAAssetPickerController.h"

#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreServices/CoreServices.h>

@interface CameraAndAlbumDemoHomeController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

/// 头像
@property (nonatomic, strong) UIButton *headButton;
/// 图片选择器
@property (nonatomic, strong) UIImagePickerController *imagePicker;

@end

@implementation CameraAndAlbumDemoHomeController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // 头像
    [self.headButton addTarget:self action:@selector(clickButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.headButton];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _headButton.frame               = CGRectMake(self.view.center.x - 50, 100, 100, 100);
//    _headButton.layer.cornerRadius  = self.headButton.height / 2;
}

#pragma mark - evnet response

- (void)clickButton {
    
    /**
     PHAsset 图片对象
     PHAssetCollection 相册对象
     PHCollectionList 相册文件夹对象
     
     PHCollectionListType
        -PHCollectionListTypeMomentList
        -PHCollectionListTypeFolder
        -PHCollectionListTypeSmartFolder
     
     PHCollectionListSubtype
        -PHCollectionListSubtypeMomentListCluster
        -PHCollectionListSubtypeMomentListYear
        -PHCollectionListSubtypeRegularFolder
        -PHCollectionListSubtypeSmartFolderEvents
        -PHCollectionListSubtypeSmartFolderFaces
        -PHCollectionListSubtypeAny
     
     PHAssetCollectionType
        -PHAssetCollectionTypeAlbum 相册
        -PHAssetCollectionTypeSmartAlbum 智能相册，系统自己分配和归纳的
        -PHAssetCollectionTypeMoment 时刻，系统自动通过时间和地点生成的分组
     
     PHAssetCollectionSubtype
        -...
     */
    
    // 获取所有图片
    {
        // PHAsset对象是一张图片的映射对象，里面包含的图片的信息，大小、是否收藏、是否隐藏等等
        PHFetchResult <PHAsset *>*result = [PHAsset fetchAssetsWithOptions:nil];
        [result enumerateObjectsUsingBlock:^(PHAsset * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSLog(@"%@", obj);
        }];
        NSLog(@"%@", result);
    }

    {
        // PHAssetCollection对象是一个相册的映射对象
        PHFetchResult <PHAssetCollection *>*result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary options:nil];
        for (PHAssetCollection *collection in result) {
            NSLog(@"%@", collection);
            PHFetchResult <PHAsset *>*result2 = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
            NSLog(@"%@", result2);
        }
    }
    
    {
        PHFetchResult <PHAsset *>*result = [PHAsset fetchAssetsWithOptions:nil];
        PHAsset *asset = result.firstObject;

        PHImageManager *manager = [PHImageManager defaultManager];

//        PHImageRequestID reuqestID = [manager requestImageDataForAsset:asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
//            UIImage *image = [UIImage imageWithData:imageData];
//            [self.headButton setImage:image forState:UIControlStateNormal];
//        }];

        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
//        options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
//        options.deliveryMode = PHImageRequestOptionsDeliveryModeOpportunistic;
        options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        [manager requestImageForAsset:asset targetSize:CGSizeMake(1000, 1000) contentMode:PHImageContentModeDefault options:options resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
            [self.headButton setImage:result forState:UIControlStateNormal];
        }];
    }
    
    {
        PHFetchResult <PHCollectionList *>*result = [PHCollectionList fetchMomentListsWithSubtype:PHCollectionListSubtypeAny options:nil];
        NSLog(@"%@", result);
        for (PHCollectionList *list in result) {
            NSLog(@"%@", list);
        }
    }
    
    {
        PHFetchResult <PHCollectionList *>*result = [PHCollectionList fetchCollectionListsWithType:PHCollectionListTypeMomentList subtype:PHCollectionListSubtypeMomentListYear options:nil];
        NSLog(@"%@", result);
        for (PHCollectionList *list in result) {
            NSLog(@"%ld-%ld", list.startDate.year, list.endDate.year);
        }
    }
    
    {
        PHFetchResult <PHCollectionList *>*result = [PHCollectionList fetchCollectionListsWithType:PHCollectionListTypeSmartFolder subtype:PHCollectionListSubtypeAny options:nil];
        NSLog(@"%@", result);
        for (PHCollectionList *list in result) {
            NSLog(@"%@", list.localizedTitle);
        }
    }
    
    {
        PHFetchResult <PHCollectionList *>*result = [PHCollectionList fetchCollectionListsWithType:PHCollectionListTypeFolder subtype:PHCollectionListSubtypeAny options:nil];
        NSLog(@"%@", result);
        for (PHCollectionList *list in result) {
            NSLog(@"%@", list.localizedTitle);
        }
    }
    
    {
        PHFetchResult <PHAssetCollection *>*result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
        NSLog(@"%@", result);
        for (PHAssetCollection *collection in result) {
            NSLog(@"%@", collection.localizedTitle);
        }
    }
    
    {
        PHFetchResult <PHAssetCollection *>*result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
        NSLog(@"%@", result);
        for (PHAssetCollection *collection in result) {
            NSLog(@"%@", collection.localizedTitle);
        }
    }
    
    return;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        // 判断设备是否支持相机
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            DebugLog(@"设备不支持打开相机");
            return;
        }
        
        // 请求相机访问权限
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                UIImagePickerController *imagePicker    = [[UIImagePickerController alloc] init];
                imagePicker.allowsEditing               = YES;
                imagePicker.delegate                    = self;
                imagePicker.sourceType                  = UIImagePickerControllerSourceTypeCamera;
                imagePicker.mediaTypes                  = @[(NSString *)kUTTypeImage];
                imagePicker.navigationBar.barTintColor  = [UIColor lightGrayColor];
                imagePicker.navigationBar.tintColor     = [UIColor whiteColor];
                [self presentViewController:imagePicker animated:YES completion:nil];
                
                PAAssetPickerController *picker = [[PAAssetPickerController alloc] init];
                [self presentViewController:picker animated:YES completion:nil];
            } else {
                DebugLog(@"无相机访问权限");
            }
        }];
    }];
    
    UIAlertAction *openPhotoLibraryAction = [UIAlertAction actionWithTitle:@"从相册中选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        // 判断设备是否支持相机
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            DebugLog(@"设备不支持打开相册");
            return;
        }
        
        // 请求相册访问权限
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    PAAssetPickerController *picker = [[PAAssetPickerController alloc] init];
                    [self presentViewController:picker animated:YES completion:nil];
                });
                
//                UIImagePickerController *imagePicker    = [[UIImagePickerController alloc] init];
//                imagePicker.allowsEditing               = YES;
//                imagePicker.delegate                    = self;
//                imagePicker.sourceType                  = UIImagePickerControllerSourceTypePhotoLibrary;
//                imagePicker.mediaTypes                  = @[(NSString *)kUTTypeImage];
//                imagePicker.navigationBar.barTintColor  = [UIColor lightGrayColor];
//                imagePicker.navigationBar.tintColor     = [UIColor whiteColor];
//                
//                [self presentViewController:imagePicker animated:YES completion:nil];
            } else {
                DebugLog(@"无相册访问权限");
            }
        }];
    }];
    [alert addAction:cancelAction];
    [alert addAction:takePhotoAction];
    [alert addAction:openPhotoLibraryAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

// 选定了图片后的回调方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    DebugLog(@"%@", info);
//    NSString *cropRect = [info valueForKey:UIImagePickerControllerCropRect];
//    NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
//    UIImage *editedImage = [info valueForKey:UIImagePickerControllerEditedImage];
    UIImage *originalImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self.headButton setBackgroundImage:originalImage forState:UIControlStateNormal];
        [self.headButton setBackgroundImage:originalImage forState:UIControlStateHighlighted];
    }];
}

// 取消选择图片的回调方法
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Picker Cancel");
    }];
}

#pragma mark - getters and setters

- (UIButton *)headButton {
    if (!_headButton) {
        _headButton                     = [UIButton buttonWithType:UIButtonTypeCustom];
        _headButton.backgroundColor     = [UIColor colorFromHexRGB:@"c0c0c0"];
        _headButton.layer.masksToBounds = YES;
        _headButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        _headButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        _headButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
    }
    return _headButton;
}

@end
