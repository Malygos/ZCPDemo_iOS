//
//  ZYQAssetPickerController.h
//  ananzu
//
//  Created by Aim on 14-7-26.
//  Copyright (c) 2014年. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
@protocol ZYQAssetViewCellDelegate;
@protocol PAAssetPickerControllerDelegate;

#pragma mark - PAAssetPickerController

@interface PAAssetPickerController : UINavigationController

@property (nonatomic, weak) id <UINavigationControllerDelegate, PAAssetPickerControllerDelegate> seldelegate;
@property (nonatomic, copy, readonly) NSArray *indexPathsForSelectedItems;
@property (nonatomic, assign) NSInteger maximumNumberOfSelection;
@property (nonatomic, assign) NSInteger minimumNumberOfSelection;
@property (nonatomic, strong) NSPredicate *selectionFilter;
@property (nonatomic, assign) BOOL showCancelButton;
@property (nonatomic, assign) BOOL showEmptyGroups;
@property (nonatomic, assign) BOOL isFinishDismissViewController;

@end

@protocol PAAssetPickerControllerDelegate <NSObject>

-(void)assetPickerController:(PAAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets;
@optional
-(void)assetPickerControllerDidCancel:(PAAssetPickerController *)picker;
-(void)assetPickerController:(PAAssetPickerController *)picker didSelectAsset:(PHAsset *)asset;
-(void)assetPickerController:(PAAssetPickerController *)picker didDeselectAsset:(PHAsset *)asset;
-(void)assetPickerControllerDidMaximum:(PAAssetPickerController *)picker;
-(void)assetPickerControllerDidMinimum:(PAAssetPickerController *)picker;

@end


#pragma mark - ZYQAssetGroupViewController

@interface PAAssetGroupViewController : UITableViewController
@end


#pragma mark - ZYQAssetViewController

@interface ZYQAssetViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) PHAssetCollection *assetsGroup;
@property (nonatomic, strong) NSMutableArray *indexPathsForSelectedItems;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic,assign) NSInteger number;     //新加的，选中的张数

@end

#pragma mark - ZYQVideoTitleView

@interface ZYQVideoTitleView : UILabel

@end

#pragma mark - ZYQTapAssetView

@protocol ZYQTapAssetViewDelegate <NSObject>

-(void)touchSelect:(BOOL)select;
-(BOOL)shouldTap;

@end

@interface ZYQTapAssetView : UIView

@property (nonatomic, assign) BOOL selected;
@property (nonatomic, assign) BOOL disabled;
@property (nonatomic, weak) id<ZYQTapAssetViewDelegate> delegate;

@end

#pragma mark - ZYQAssetView

@protocol ZYQAssetViewDelegate <NSObject>

-(BOOL)shouldSelectAsset:(PHAsset*)asset;
-(void)tapSelectHandle:(BOOL)select asset:(PHAsset*)asset;

@end

@interface ZYQAssetView : UIView

- (void)bind:(PHAsset *)asset selectionFilter:(NSPredicate *)selectionFilter isSeleced:(BOOL)isSeleced;

@end

#pragma mark - ZYQAssetViewCell

@interface ZYQAssetViewCell : UITableViewCell

@property(nonatomic,weak)id<ZYQAssetViewCellDelegate> delegate;

- (void)bind:(NSArray *)assets selectionFilter:(NSPredicate*)selectionFilter minimumInteritemSpacing:(float)minimumInteritemSpacing minimumLineSpacing:(float)minimumLineSpacing columns:(int)columns assetViewX:(float)assetViewX;

@end

@protocol ZYQAssetViewCellDelegate <NSObject>

- (BOOL)shouldSelectAsset:(PHAsset *)asset;
- (void)didSelectAsset:(PHAsset *)asset;
- (void)didDeselectAsset:(PHAsset *)asset;

@end

#pragma mark - ZYQAssetGroupViewCell

@interface ZYQAssetGroupViewCell : UITableViewCell

- (void)bind:(PHAssetCollection *)assetsGroup;

@end
