//
//  CameraAndAlbumDemoHomeController.h
//  CameraAndAlbumDemo
//
//  Created by apple on 16/3/1.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 访问相机、相册demo
 */
@interface CameraAndAlbumDemoHomeController : ZCPBaseViewController

@end
