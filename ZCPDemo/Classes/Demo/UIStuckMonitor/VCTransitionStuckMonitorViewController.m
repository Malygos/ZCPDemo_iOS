//
//  VCTransitionStuckMonitorViewController.m
//  ZCPDemo
//
//  Created by bobo on 2020/4/25.
//  Copyright © 2020年 zcp. All rights reserved.
//

#import "VCTransitionStuckMonitorViewController.h"
#import "SMLagMonitor.h"

@interface VCTransitionStuckMonitorViewController ()

@end

@implementation VCTransitionStuckMonitorViewController

- (void)viewDidLoad {
    //kCFRunLoopBeforeWaiting
    [[SMLagMonitor shareInstance] monitorBeforeWaitingBlock:^{
        [super viewDidLoad];
        self.view.backgroundColor = [UIColor whiteColor];
        [self doSomething];
    }];
}

- (void)willMoveToParentViewController:(nullable UIViewController *)parent {
    NSLog(@"willMoveToParentViewController parent: %@", parent);
    // EnterPage kCFRunLoopBeforeWaiting no
    // LeavePage kCFRunLoopBeforeSources ok
}
- (void)didMoveToParentViewController:(nullable UIViewController *)parent {
    NSLog(@"didMoveToParentViewController parent: %@", parent);
    // EnterPage kCFRunLoopAfterWaiting ok
    // LeavePage kCFRunLoopAfterWaiting ok
}

- (void)viewWillAppear:(BOOL)animated {
    // kCFRunLoopBeforeWaiting
    [[SMLagMonitor shareInstance] monitorBeforeWaitingBlock:^{
        [super viewWillAppear:animated];
        NSLog(@"viewWillAppear");
        [self doSomething];
    }];
}
- (void)viewDidAppear:(BOOL)animated {
    //kCFRunLoopAfterWaiting ok
    NSLog(@"viewDidAppear");
    [super viewDidAppear:animated];
//    [self doSomething];
}
- (void)viewWillDisappear:(BOOL)animated {
    // kCFRunLoopBeforeWaiting
    [[SMLagMonitor shareInstance] monitorBeforeWaitingBlock:^{
        NSLog(@"viewWillDisappear");
        [super viewWillDisappear:animated];
        [self doSomething];
    }];
}
- (void)viewDidDisappear:(BOOL)animated {
    //kCFRunLoopBeforeSources
    NSLog(@"viewDidDisappear");
    [super viewDidDisappear:animated];
//    [self doSomething];
}

#pragma mark - test

- (void)doSomething {
    NSLog(@"执行了一个繁重的任务");
    sleep(2);
    NSLog(@"繁重的任务执行完毕");
}

@end
