//
//  ScrollStuckMonitorViewController.m
//  ZCPDemo
//
//  Created by bobo on 2020/4/25.
//  Copyright © 2020年 zcp. All rights reserved.
//

#import "ScrollStuckMonitorViewController.h"
#import "SMLagMonitor.h"
#import <ZCPListView/ZCPListView.h>

@interface ScrollStuckMonitorViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSString *imageUrl;

@end

@implementation ScrollStuckMonitorViewController

- (void)viewDidLoad {
    [[SMLagMonitor shareInstance] monitorBeforeWaitingBlock:^{
        [super viewDidLoad];
        self.view.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:self.tableView];
        [self loadData];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [[SMLagMonitor shareInstance] monitorBeforeWaitingBlock:^{
        [super viewWillAppear:animated];
    }];
}

-  (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.tableView.frame = self.view.bounds;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

#pragma mark - load

- (void)loadData {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.imageUrl = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1587813618158&di=af983e1ebbc81d482a05b73aeadcfcb2&imgtype=0&src=http%3A%2F%2Fa0.att.hudong.com%2F64%2F76%2F20300001349415131407760417677.jpg";
        [self.tableView reloadData];
    });
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [[SMLagMonitor shareInstance] monitorBeforeWaitingBlock:^{
        // 初始化时这里面追踪不到。方法执行在kCFRunLoopBeforeWaiting之后kCFRunLoopAfterWaiting之前
        // 方法执行在VC的几个生命周期方法之后（viewDidLoad viewWillAppear willLayout didLayout）
        // 但是对于一般的tableVC，都是在网络请求之后才会显示cell，所以一般情况下此处方法调用会在viewDidAppear之后，可以被AfterWaiting监控到
        // 上面说的不对，有可能AfterWaiting监控不到。dispatch_after延时之后，就是卡在BeforeWaiting之后
        NSURL *imageURL = [NSURL URLWithString:self.imageUrl];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        cell.imageView.image = [UIImage imageWithData:imageData];
    }];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

#pragma mark - getters

- (UITableView *)tableView  {
    if (!_tableView) {
        _tableView = [UITableView zcp_new];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}

@end
