//
//  UIStuckMonitorDemoHomeViewController.m
//  ZCPDemo
//
//  Created by bobo on 2020/4/25.
//  Copyright © 2020年 zcp. All rights reserved.
//

#import "UIStuckMonitorDemoHomeViewController.h"
#import "VCTransitionStuckMonitorViewController.h"
#import "ScrollStuckMonitorViewController.h"

@implementation UIStuckMonitorDemoHomeViewController

@synthesize infoArr = _infoArr;

- (NSMutableArray *)infoArr {
    if (_infoArr == nil) {
        _infoArr = [NSMutableArray arrayWithObjects:
                    @{@"title": @"VC转场过渡卡顿", @"class": @"VCTransitionStuckMonitorViewController"},
                    @{@"title": @"滑动卡顿",      @"class": @"ScrollStuckMonitorViewController"},
                    nil];
    }
    return _infoArr;
}

@end
