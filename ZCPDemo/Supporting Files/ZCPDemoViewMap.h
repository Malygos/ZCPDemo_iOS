//
//  ZCPDemoViewMap.h
//  Demo
//
//  Created by 朱超鹏 on 2017/10/10.
//  Copyright © 2017年 zcp. All rights reserved.
//

#ifndef ZCPDemoViewMap_h
#define ZCPDemoViewMap_h

#pragma mark - home

// 首页 ZCPDemoHomeViewController
#define APPURL_VIEW_IDENTIFIER_HOME                 @"home"
// 临时测试 TemporaryTestHomeController
#define APPURL_VIEW_IDENTIFIER_TEMPTESTHOME         @"tempTestHome"
// ui样例首页 UIDemoHomeController
#define APPURL_VIEW_IDENTIFIER_UIHOME               @"uiHome"
// web容器 ZCPDemoWebViewController
#define APPURL_VIEW_IDENTIFIER_WEBHOME              @"webHome"
// UIPageViewController PageVCDemoHomeController
#define APPURL_VIEW_IDENTIFIER_PAGEVCHOME           @"pageVCHome"
// 相机/相册 CameraAndAlbumDemoHomeController
#define APPURL_VIEW_IDENTIFIER_ALBUMHOME            @"albumHome"
// 其他 OtherDemoHomeController
#define APPURL_VIEW_IDENTIFIER_OTHERHOME            @"otherHome"
// 图片轮播 PhotoCarouselDemoHomeController
#define APPURL_VIEW_IDENTIFIER_PHOTOCAROUSELHOME    @"photoCarouselHome"
// tabbar TabBarDemoHomeController
#define APPURL_VIEW_IDENTIFIER_TABBARHOME           @"tabbarHome"
// 网络 NetWorkDemoHomeController
#define APPURL_VIEW_IDENTIFIER_NETWORKHOME          @"networkHome"
// 二维码扫描 QRCodeDemoHomeController
#define APPURL_VIEW_IDENTIFIER_QRCODEHOME           @"qrCodeHome"
// 国际化 InternationalizationDemoHomeController
#define APPURL_VIEW_IDENTIFIER_INTERNATIONALHOME    @"internationalHome"
// 地图 MapDemoHomeViewController
#define APPURL_VIEW_IDENTIFIER_MAPHOME              @"mapHome"
// 扫雷 ZCPMinesweeperViewController
#define APPURL_VIEW_IDENTIFIER_MINESWEEPER          @"minesweeper"
// 日志 LogDemoHomeViewController
#define APPURL_VIEW_IDENTIFIER_LOG                  @"log"
// 锁 LockDemoHomeViewController
#define APPURL_VIEW_IDENTIFIER_LOCK                 @"lock"
// Gesture GestureDemoHomeViewController
#define APPURL_VIEW_IDENTIFIER_GESTURE              @"gesture"
// UI卡顿监控
#define APPURL_VIEW_IDENTIFIER_UISTUCKMONITOR       @"UIStuckMonitor"

#pragma mark - UI

// 文字适配 SizeToFitDemoHomeController
#define APPURL_VIEW_IDENTIFIER_SIZETOFIT            @"sizeToFit"
// 画板 PaletteDemoHomeController
#define APPURL_VIEW_IDENTIFIER_PALETTE              @"palette"
// 字体 FontDemoHomeControllerViewController
#define APPURL_VIEW_IDENTIFIER_FONT                 @"font"
// 气泡拉伸 QiPaoDemoHomeController
#define APPURL_VIEW_IDENTIFIER_QIPAO                @"qipao"
// 虚线 DashedDemoHomeController
#define APPURL_VIEW_IDENTIFIER_DASHED               @"dashed"
// collection CollectionViewDemoHomeController
#define APPURL_VIEW_IDENTIFIER_COLLECTION           @"collection"
// alert AlertViewDemoController
#define APPURL_VIEW_IDENTIFIER_ALERT                @"alert"

#pragma mark - Animation

// CoreAnimation CoreAnimationDemoViewController
#define APPURL_VIEW_IDENTIFIER_COREANIMATION        @"coreAnimation"
// 动画首页 AnimationDemoHomeViewController
#define APPURL_VIEW_IDENTIFIER_ANIMATION            @"animationHome"
// UIHomeDemo CATransform3DDemoHomeController
#define APPURL_VIEW_IDENTIFIER_CATRANSFORM3D        @"catransform3d"
// 波浪线 WaveLineDemoHomeController
#define APPURL_VIEW_IDENTIFIER_WAVELINE             @"waveLine"
// 爆炸效果 ExplodeViewController
#define APPURL_VIEW_IDENTIFIER_EXPLODE              @"explode"
// 橡皮筋效果 UIElasticViewController
#define APPURL_VIEW_IDENTIFIER_ELASTIC              @"elastic"
// 钟表 ClockViewController
#define APPURL_VIEW_IDENTIFIER_CLOCK                @"clock"
// 动画按钮 AnimationButtonViewController
#define APPURL_VIEW_IDENTIFIER_ANIMATIONBUTTON      @"animationButton"

#pragma mark - GUI

#define APPURL_VIEW_IDENTIFIER_LAYOUTDEMO           @"layoutDemo"

#endif /* ZCPDemoViewMap_h */
