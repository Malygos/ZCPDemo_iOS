//
//  AppDelegate.m
//  ZCPDemo
//
//  Created by zhuchaopeng06607 on 2020/2/4.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "AppDelegate.h"
#import "AppManager.h"
#import "DebugManager.h"
#import "ZCPControllerFactory+Category.h"
#ifdef DEBUG
#import <DoraemonKit/DoraemonManager.h>
#import "SMLagMonitor.h"
#endif

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    NSString *logPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/log"];
    
    NSLog(@"%@", logPath);

    /// view map
    [ZCPNavigator readViewControllerMapWithViewMapNamed:@"demoViewMap"];
    /// vc stack
    UINavigationController *nav = [[ZCPControllerFactory sharedInstance] generateCustomStack];
    [[ZCPNavigator sharedInstance] setupRootViewController:nav];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [ZCPNavigator sharedInstance].rootViewController;
    [self.window makeKeyAndVisible];
    
    [DebugManager defaultManager].alwaysShowStatusBall = YES;
    
//    [AppManager checkAppVersion];
//    [AppManager checkAppVersion_custom];
    
#ifdef DEBUG
    [self injectionIII];
    [[DoraemonManager shareInstance] install];
    [[SMLagMonitor shareInstance] beginMonitor];
#endif
    
    return YES;
}

- (void)injectionIII {
    NSString *sandBoxPath = NSHomeDirectory();
    NSArray *paths = [sandBoxPath componentsSeparatedByString:@"/Library/Developer/CoreSimulator/Devices"];
    NSString *macUserPath = (paths.count > 0) ? paths[0] : @"";
    NSString *standardComputerInjectionIIIPath = [NSString stringWithFormat:@"%@/Downloads/Applications/InjectionIII.app/Contents/Resources/iOSInjection.bundle", macUserPath];
    NSString *defaultInjectionIIIPath = @"/Applications/InjectionIII.app/Contents/Resources/iOSInjection.bundle";
    [[NSBundle bundleWithPath:standardComputerInjectionIIIPath] load];
    [[NSBundle bundleWithPath:defaultInjectionIIIPath] load];
}


- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

@end
