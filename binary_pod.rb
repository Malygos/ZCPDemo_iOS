# 二方库（二进制）
def binary_pod(pod_name, version=nil, **args)
    git = args[:git]
    branch = args[:branch]
    tag = args[:tag]
    commit = args[:commit]
    configurations = args[:configurations]
    
    args[:source] = "https://gitlab.com/Malygos/zcp-repos-binary.git"

    pod pod_name, version, args
end

# 三方库（二进制）
def third_binary_pod(pod_name, version=nil, **args)
end

# 二方库（源码）
def source_pod(pod_name, **args)
	unless args[:path]
		args[:git] = "https://gitlab.com/Malygos/#{pod_name}.git"
	end
	pod pod_name, args
end